"""This module handles the generation of Graph Neural Networks (GNNs) of two types:
* homogeneous graph neural networks
* heterogeneous graph neural networks
The networks are build using PyG (PyTorch Geometric), a library built upon PyTorch to easily write and train GNNs.
Each GNN consists of three parts:
* A graph convolutional part, handling graph structured data
* A global dense NN, handling "regular" input data
* A final dense NN combining the forementioned graph and global outputs.

"""
import HelperModules.helperfunctions as hf
import numpy as np
import torch
from HelperModules.graphbuilder import graphbuilder
from ModelModules.basetorchmodel import basetorchmodel, get_pytorch_act
from torch.nn import (BatchNorm1d, Dropout, Linear)
from torch_geometric.loader import DataLoader
from torch_geometric.nn import (GATConv, GCNConv, HeteroConv, SAGEConv,
                                Sequential, global_max_pool, global_mean_pool)


def buildgeomfc(in_nodes, hidden_nodes, hidden_act, out_nodes=None, out_act=None, input_string='x', config=None):
    """Method to create a fully connected (PyTorch) NN. We have to use this instead of the base version.

    :param in_nodes: Number of input nodes.
    :param hidden_nodes: List of integers defining the number of nodes per hidden layer.
    :param hidden_act: List of strings containing identifiers for activation functions in the hidden layers.
    :param out_nodes: Number of output nodes.
    :param out_act: String identifier for output activation function.
    :param input_string: String to define the inputs for Sequential_geometric object.
    :returns: PyTorch Sequential object

    """
    fcmodel = []
    prevnodes = in_nodes
    layerid = 1
    for nodes, act in zip(hidden_nodes, hidden_act):
        fcmodel.append((Linear(prevnodes, nodes), f'{input_string} -> {input_string}'))
        fcmodel.append(get_pytorch_act(act))
        prevnodes = nodes
        if layerid in config.get("MODEL").get("DropoutIndece"):
            fcmodel.append(Dropout(config.get("MODEL").get("DropoutProb")))
        if layerid in config.get("MODEL").get("BatchNormIndece"):
            fcmodel.append(BatchNorm1d(nodes))
        layerid += 1
    if out_nodes is not None:
        fcmodel.append((Linear(prevnodes, out_nodes), f'{input_string} -> {input_string}'))
    if out_act is not None:
        fcmodel.append(get_pytorch_act(out_act))
    return Sequential(f'{input_string}', fcmodel)


class gnnmodel(basetorchmodel):
    """Class to build a homogeneous or heterogeneous GNN.
    The GNN is build using the constructors of the homognnmodel or heterognnmodel classes.

    """

    def __init__(self, cfgset):
        super().__init__(cfgset)
        if self.m_cfgset.get("MODEL").get("GraphStructure") == "homogeneous":
            self.m_model = homognnmodel(self.m_cfgset,
                                        self._getglobal())
        elif self.m_cfgset.get("MODEL").get("GraphStructure") == "heterogeneous":
            self.m_model = heterognnmodel(self.m_cfgset,
                                          self._getglobal(),
                                          self._getedgetypes())
        else:
            self.m_model = None
        self.m_gbuilder = graphbuilder(self.m_cfgset.get("GRAPHNODE"), self.m_cfgset.get("MODEL").get("GraphStructure"))

    def _getglobal(self):
        """Getter methond to determine the number of global input features of a GNN.

        :returns: Number of global input features of a GNN.

        """
        for node in self.m_cfgset.get("GRAPHNODE"):
            if node.get("Type") == "Global":
                return len(node.get("Features"))
        return 0

    def _getedgetypes(self):
        """Getter method to get a list of different connection types for a heterogeneous GNN.
        An example of a connection could be ('Jet', 'Jet__to__Lepton', 'Lepton') connecting
        two objects named 'Jet' and 'Lepton'  respectively.

        :returns: List of connection types.

        """
        edgetypes = []
        nodemap = {}
        for node in self.m_cfgset.get("GRAPHNODE"):
            if node.get("Type") != "Global":
                nodemap[node.get("Name")] = {"Type": node.get("Type"),
                                             "Node": node}
        for node in self.m_cfgset.get("GRAPHNODE"):
            if node.get("Type") != "Global":
                sourceType = node.get("Type")
                for target in node.get("Targets"):
                    targetType = nodemap[target]["Type"]
                    edgetype = (sourceType, sourceType + "_to_" + targetType, targetType)
                    if edgetype not in edgetypes:
                        edgetypes.append(edgetype)
        return edgetypes

    def forward(self, data):
        """PyTorch forward pass method

        :returns: Model predictions

        """
        return self.m_model(data)

    def run(self, x, y, w):
        """Runner method for a GNN.
        The method will take the input data (x,y,w) and use a graphbuilder object to constructs graphs from it.
        These graphs are parsed to DataLoader objects, and finally the GNN is trained using those.

        :param x: Input data in pd.DataFrame format.
        :param y: Targets in pd.DataFrame format.
        :param w: Weights in pd.DataFrame format.
        :returns:

        """
        data = hf.prepinputs(x, y, w, self.m_cfgset, library="pd")
        xtrain_dict, xval_dict = data["xtrain"].to_dict('records'), data["xval"].to_dict('records')
        train_graphs = self.m_gbuilder.buildgraphs(xtrain_dict, data["ytrain"].values, data["wtrain"].values)
        val_graphs = self.m_gbuilder.buildgraphs(xval_dict, data["yval"].values, data["wval"].values)
        trainloader = DataLoader(train_graphs,
                                 batch_size=self.m_cfgset.get("MODEL").get("BatchSize"),
                                 shuffle=True)
        valloader = DataLoader(val_graphs,
                               batch_size=self.m_cfgset.get("MODEL").get("BatchSize"),
                               shuffle=True)
        self.m_model, history = self.runepochs(self.m_model, trainloader, valloader)
        return self.m_model, history

    def _trainstep(self, model, loader):
        """Function to run training epoch overriding the parent method.

        :param trainloader: pytorch DataLoader object
        :returns: Loss of the current epoch

        """
        self.setlossfct()
        optimizer = self.setoptimiser(model)
        model.train()
        running_loss = 0
        tot = len(loader.dataset)
        for data in loader:  # Iterate in batches over the training dataset.
            data.to(self.m_device)
            optimizer.zero_grad()  # zero the parameter gradients.
            out = model(data)  # Perform a single forward pass.
            loss = torch.mul(self.get_loss(out, data.y, data.w), len(data) / tot)
            loss.backward()  # Derive gradients.
            optimizer.step()  # Update parameters based on gradients.
            running_loss += loss.item()
        return running_loss

    def _teststep(self, model, loader):
        """Function to run testing epoch overriding the parent method.

        :param trainloader: pytorch DataLoader object
        :returns: Loss of the current epoch

        """
        running_loss = 0
        tot = len(loader.dataset)
        for data in loader:  # Iterate in batches over the validation dataset.
            data.to(self.m_device)
            out = model(data)  # Perform a single forward pass.
            loss = torch.mul(self.get_loss(out, data.y, data.w), len(data) / tot)
            running_loss += loss.item()
        return running_loss


class homoconvmodel(torch.nn.Module):
    """Class for a convolutional model for a homogeneous GNN.
    A 'homoconvmodel' takes convolutional layers and stack them.
    The available convulutional layers are taken from:
    https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html
    """

    def __init__(self, glayers, gact, gchannels):
        super().__init__()
        convmodel, prevch = [], -1
        for layer, act, channels in zip(glayers, gact, gchannels):
            if layer == "GCNConv":
                convmodel.append((GCNConv(prevch, channels), 'x, edge_index -> x'))
            elif layer == "SAGEConv":
                convmodel.append((SAGEConv(prevch, channels), 'x, edge_index -> x'))
            elif layer == "GATConv":
                convmodel.append((GATConv(prevch, channels), 'x, edge_index, edge_attr -> x'))
            prevch = channels
            convmodel.append(get_pytorch_act(act))
        self.convmodel = Sequential('x, edge_index, edge_attr', convmodel)

    def forward(self, x, edge_index, edge_attr):
        """Forward method for homogeneous graph convolutional model.

        :param x: (PyTorch Tensor), [N, F_x], where N is the number of nodes and F_x the number of node features.
        :param edge_index: (PyTorch Tensor), [2, E] with max entry N - 1, where E is the number of edges.
        :param edge_attr: (PyTorch Tensor): [E, F_e] with F_e being the edge features
        :returns: (PyTorch Tensor), The output of the convolutional model.
        """
        return self.convmodel(x, edge_index, edge_attr)


class homognnmodel(basetorchmodel):
    """Class for a homogeneous GNN.
    Takes a homogeneous convolutional model and combines it with a dense NN.
    Finally the convolutional output and the dense output are combined using:
    * the global mean of the convolutional layers
    * the global maximum of the convolutional layers
    * the output of the global dense network

    """

    def __init__(self, cfgset, in_nodes):
        super().__init__(cfgset)

        self.convmodel = homoconvmodel(glayers=self.m_cfgset.get("MODEL").get("GraphLayers"),
                                       gact=self.m_cfgset.get("MODEL").get("GraphActivationFunctions"),
                                       gchannels=self.m_cfgset.get("MODEL").get("GraphChannels"))
        self.glob = buildgeomfc(in_nodes=in_nodes,
                                hidden_nodes=self.m_cfgset.get("MODEL").get("GlobalNodes"),
                                hidden_act=self.m_cfgset.get("MODEL").get("GlobalActivationFunctions"),
                                config=self.m_cfgset)
        self.fcmodel = buildgeomfc(in_nodes=self.m_cfgset.get("MODEL").get("GlobalNodes")[-1] + self.m_cfgset.get("MODEL").get("GraphChannels")[-1] * 2,
                                   hidden_nodes=self.m_cfgset.get("MODEL").get("FinalNodes"),
                                   hidden_act=self.m_cfgset.get("MODEL").get("FinalActivationFunctions"),
                                   out_nodes=len(self.m_cfgset.get("OUTPUT")),
                                   out_act=self.m_cfgset.get("MODEL").get("OutputActivation"),
                                   config=self.m_cfgset)

    def forward(self, data):
        """PyTorch forward pass method

        :param data: A pytorch data object describing a homogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        :returns:

        """
        x_new = self.convmodel(data.x.float(), data.edge_index, data.edge_attr)
        u_new = self.glob(data.u.float())
        x_mean_pool, x_max_pool = global_mean_pool(x_new, data.batch), global_max_pool(x_new, data.batch)
        out = torch.cat([u_new, x_mean_pool, x_max_pool], dim=1).float()
        return self.fcmodel(out)


class heteroconvmodel(torch.nn.Module):
    """
    Class for a convolutional mode for a heterogeneous graph convolutions.
    We use the generic wrapper for computing graph convolution on heterogeneous graphs.
    The 'HeteroConv' layer will pass messages from source nodes to target nodes based on the bipartite GNN layer given for a specific edge type.
    The results will be aggregated using the (default) sum function.
    """

    def __init__(self, glayers, gact, gchannels, edgetypes):
        """Constructur of heteroconvmodel class. Creates a sequential set of graph convolutions.
        The used convolutions are defined via 'glayers'.
        Their activation functions are defined via 'gact'.

        :param: glayers: List of strings containing identifiers for the convolutional layers.
        :param: grap_act: List of strings containing the identifiers for the activation functions.
        :param: gchannels: List of Integers defining the hidden channels.
        :param: edgetypes: List of edge types.
        :returns:

        """
        super().__init__()
        convmodel = []
        for layer, _, channels in zip(glayers, gact, gchannels):
            if layer == "GCNConv":
                raise NotImplementedError
            if layer == "SAGEConv":
                convmodel.append((HeteroConv({
                    edgetype: SAGEConv((-1, -1), channels)
                    for edgetype in edgetypes
                }), 'x_dict, edge_index_dict -> x_dict'))
            elif layer == "GATConv":
                convmodel.append((HeteroConv({
                    edgetype: GATConv((-1, -1), channels, add_self_loops=False)
                    for edgetype in edgetypes
                }), 'x_dict, edge_index_dict, edge_attr_dict -> x_dict'))
        self.convmodel = Sequential('x_dict, edge_index_dict, edge_attr_dict', convmodel)

    def forward(self, x_dict, edge_index_dict, edge_attr_dict):
        """PyTorch forward pass method

        :param x_dict: {Node type:[N, F_x]}, where N is the number of nodes and F_x the number of node features.
        :param edge_index_dict: {Node type:[2, E]} with max entry N - 1, where E is the number of edges.
        :returns: The output of the heterogeneous graph convolution model

        """
        x_dict = self.convmodel(x_dict, edge_index_dict, edge_attr_dict)
        return x_dict


class heterognnmodel(basetorchmodel):
    """
    Takes a heterogeneous convolutional model and combines it with a dense NN.
    Finally the convolutional output and the dense output are combined using:
    * the iundividual global mean of the convolutional layers separated into edge types.
    * the output of the global dense network
    """

    def __init__(self, cfgset, in_nodes, edgetypes):
        super().__init__(cfgset)

        nodetypes = np.unique([i[0] for i in edgetypes])
        self.convmodel = heteroconvmodel(glayers=self.m_cfgset.get("MODEL").get("GraphLayers"),
                                         gact=self.m_cfgset.get("MODEL").get("GraphActivationFunctions"),
                                         gchannels=self.m_cfgset.get("MODEL").get("GraphChannels"),
                                         edgetypes=edgetypes)
        self.glob = buildgeomfc(in_nodes=in_nodes,
                                hidden_nodes=self.m_cfgset.get("MODEL").get("GlobalNodes"),
                                hidden_act=self.m_cfgset.get("MODEL").get("GlobalActivationFunctions"),
                                config=self.m_cfgset)
        self.fcmodel = buildgeomfc(in_nodes=self.m_cfgset.get("MODEL").get("GlobalNodes")[-1] + self.m_cfgset.get("MODEL").get("GraphChannels")[-1] * len(nodetypes),
                                   hidden_nodes=self.m_cfgset.get("MODEL").get("FinalNodes"),
                                   hidden_act=self.m_cfgset.get("MODEL").get("FinalActivationFunctions"),
                                   out_nodes=len(self.m_cfgset.get("OUTPUT")),
                                   out_act=self.m_cfgset.get("MODEL").get("OutputActivation"),
                                   config=self.m_cfgset)

    def forward(self, data):
        """PyTorch forward pass method

        :param data: A pytorch data object describing a heterogeneous graph. The data object can hold node-level, link-level and graph-level attributes.
        :returns:

        """
        dataKeys = data.metadata()[0]
        x_dict = self.convmodel(data.x_dict, data.edge_index_dict, data.edge_attr_dict)
        x_dict = {key: global_mean_pool(x_dict[key], data[key].batch) for key in dataKeys}
        x_dict = torch.cat(list(x_dict.values()), dim=1)
        u_new = self.glob(data.u.float())
        out = torch.cat([u_new, x_dict], dim=1).float()
        return self.fcmodel(out)
