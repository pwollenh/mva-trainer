""" Module handling the conversion of models to onnx

"""
import warnings
import numpy as np
import torch
from skl2onnx import convert_sklearn
from skl2onnx.common.data_types import FloatTensorType


class onnxhandler():
    """
    Class to handle the conversion of models to onnx
    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_sampleinput = self._getsampleinput()

    def _getsampleinput(self):
        if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
            return torch.tensor(np.array([np.random.rand(len(self.m_cfgset.get("VARIABLE"))).astype(np.float32)]))
        if "BDT" in self.m_cfgset.get("MODEL").get("Type"):
            return [('float_input', FloatTensorType([None, len(self.m_cfgset.get("VARIABLE"))]))]

    def _convertDNN(self, model, onnxpath):
        """Method to convert a PyTorch DNN to onnx

        :param model:
        :param onnxpath:
        :returns:

        """
        torch.onnx.export(model,  # model being run
                          self.m_sampleinput,  # model input (or a tuple for multiple inputs)
                          onnxpath,  # where to save the model (can be a file or file-like object)
                          export_params=True,  # store the trained parameter weights inside the model file
                          opset_version=self.m_cfgset.get("MODEL").get("ONNXVersion"),  # the ONNX version to export the model to
                          do_constant_folding=True,  # whether to execute constant folding for optimization
                          input_names=['input'],   # the model's input names
                          output_names=['output'],  # the model's output names
                          dynamic_axes={'input': {0: 'batch_size'},    # variable length axes
                                        'output': {0: 'batch_size'}})

    def _convertBDT(self, model, onnxpath):
        """Method to convert a PyTorch BDT to onnx

        :param model:
        :param onnxpath:
        :returns:

        """
        onx = convert_sklearn(model, initial_types=self.m_sampleinput)
        with open(onnxpath, "wb") as f:
            f.write(onx.SerializeToString())

    def convert(self, model, onnxpath):
        """Main method to convert models to onnx

        :param model: Model object
        :param onnxpath: File path to onnx object
        :returns:

        """
        if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
            self._convertDNN(model, onnxpath)
        if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
            warnings.warn("ONNX conversion support is not yet implemented for GNNs")
        if "BDT" in self.m_cfgset.get("MODEL").get("Type"):
            self._convertBDT(model, onnxpath)
