"""This module handles the generation of Boosted Decision Trees (BDTs).
The network is build using scikit-learn. Find the documentation here:
https://scikit-learn.org/stable/

"""

import HelperModules.helperfunctions as hf
import numpy as np
import sklearn
from packaging import version
from sklearn.ensemble import (GradientBoostingClassifier,
                              GradientBoostingRegressor)
from sklearn.metrics import log_loss, mean_absolute_error, mean_squared_error


class bdtmodel():
    """Standard bdt class

    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_rstate = np.random.randint(1000, size=1)[0]
        self.m_model = self._buildbdt()

    def loss(self, y, yhat, w):
        """compilation of loss functions for bdt

        :param y: target
        :param yhat: prediction
        :param w: weight
        :returns: evaluated loss function

        """
        if self.m_cfgset.get("MODEL").get("Loss") == "log_loss" or self.m_cfgset.get("MODEL").get("Loss") == "deviance":
            return log_loss(y, yhat, sample_weight=w)
        if self.m_cfgset.get("MODEL").get("Loss") == "square_error":
            return mean_squared_error(y, yhat, sample_weight=w)
        return mean_absolute_error(y, yhat, sample_weight=w)

    def _buildbdt(self):
        """Method to build a bdt based on config parameters

        :returns:

        """
        if version.parse(sklearn.__version__) >= version.parse("1.1.0") and self.m_cfgset.get("MODEL").get("Loss") == "deviance":
            loss = "log_loss"  # deviance is depricated in sklearn as of 1.3, we replace it starting with v1.1
        elif version.parse(sklearn.__version__) < version.parse("1.1.0") and self.m_cfgset.get("MODEL").get("Loss") == "log_loss":
            loss = "deviance"  # log loss was introduced in 1.1.0
        else:
            loss = self.m_cfgset.get("MODEL").get("Loss")
        if self.m_cfgset.get("MODEL").get("Type") == "Regression-BDT":
            return GradientBoostingRegressor(n_estimators=self.m_cfgset.get("MODEL").get("nEstimators"),
                                             learning_rate=self.m_cfgset.get("MODEL").get("LearningRate"),
                                             loss=loss,
                                             criterion=self.m_cfgset.get("MODEL").get("Criterion"),
                                             min_samples_split=self.m_cfgset.get("MODEL").get("MinSamplesSplit"),
                                             min_samples_leaf=self.m_cfgset.get("MODEL").get("MinSamplesLeaf"),
                                             max_depth=self.m_cfgset.get("MODEL").get("MaxDepth"),
                                             max_features=self.m_cfgset.get("MODEL").get("MaxFeatures"),
                                             n_iter_no_change=self.m_cfgset.get("MODEL").get("Patience"),
                                             validation_fraction=self.m_cfgset.get("MODEL").get("ValidationSize"),
                                             tol=self.m_cfgset.get("MODEL").get("MinDelta"),
                                             verbose=self.m_cfgset.get("MODEL").get("Verbosity"),
                                             random_state=self.m_rstate)
        if self.m_cfgset.get("MODEL").get("Type") == "Classification-BDT":
            return GradientBoostingClassifier(n_estimators=self.m_cfgset.get("MODEL").get("nEstimators"),
                                              learning_rate=self.m_cfgset.get("MODEL").get("LearningRate"),
                                              loss=loss,
                                              criterion=self.m_cfgset.get("MODEL").get("Criterion"),
                                              min_samples_split=self.m_cfgset.get("MODEL").get("MinSamplesSplit"),
                                              min_samples_leaf=self.m_cfgset.get("MODEL").get("MinSamplesLeaf"),
                                              max_depth=self.m_cfgset.get("MODEL").get("MaxDepth"),
                                              max_features=self.m_cfgset.get("MODEL").get("MaxFeatures"),
                                              n_iter_no_change=self.m_cfgset.get("MODEL").get("Patience"),
                                              validation_fraction=self.m_cfgset.get("MODEL").get("ValidationSize"),
                                              tol=self.m_cfgset.get("MODEL").get("MinDelta"),
                                              verbose=self.m_cfgset.get("MODEL").get("Verbosity"),
                                              random_state=self.m_rstate)
        return None

    def run(self, x, y, w):
        """Runner method for a BDT.
        Takes input data to train a BDT.

        :param x: Input data in pd.DataFrame format
        :param y: Targets in pd.DataFrame format
        :param w: Weights in pd.DataFrame format
        :param fold: Fold
        :returns:

        """
        data = hf.prepinputs(x, y, w, self.m_cfgset, library="pd", randomstate=self.m_rstate)
        self.m_model.fit(data["xtrain"].values,
                         data["ytrain"].values.T[0],
                         sample_weight=data["wtrain"].values.T[0])
        valloss = []
        for _, ypred in enumerate(self.m_model.staged_predict_proba(data["xval"].values)):
            valloss.append(self.loss(
                data["yval"].values.T[0],
                ypred,
                data["wval"].values.T[0]))
        # when sklearn uses log_loss internally it actually uses Binomialdeviance
        # Hence we correct this by multiplying a 0.5
        if len(self.m_cfgset.get("OUTPUT")) == 1:
            correction = 0.5
        else:
            correction = 1.0
        history = {"loss": self.m_model.train_score_ * correction,
                   "val_loss": np.array(valloss)}
        return self.m_model, history
