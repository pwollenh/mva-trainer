"""Collection of useful miscelleanous functions

"""

import os

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


def absfpaths(directory):
    """Method to get the absolute file paths w.r.t. a given directory path

    :param directory: path to directory
    :returns:

    """

    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            yield os.path.abspath(os.path.join(dirpath, f))


def addslash(path):
    """
    Enusure a given path endes with a '/'.

    Parameters:
    path -- path to be checked

    returns path with '/' at the end.
    """
    if path[-1] == "/":
        return path
    return path + "/"


def char_isAllowed(c):
    """Function to check whether a char is an allowed char to be used in a filepath.

    :param c: input char
    :returns: Returns True if char c is allowed, False otherwise

    """
    return c.isalnum() or c in " _-.,[]()"


def filter_VarPathName(var):
    """Filters out illegal chars from variable names for filename usage

    For variable Ctrl-plots, some characters are illegal in the filename.
    This function replaces the illegal chars in `__pathCharReplDict` by
    the strings specified there and removes all other chars not allowed
    in `__char_isAllowed`.

    :param string: Variable name to be filtered by the rules outlined above.
    :returns: Filtered variable name suitable for filenames.

    """
    pathCharReplDict = {
        "/": "_DIV_",
        "*": "_TIMES_"
    }
    varpath = var
    # Replacing chars by their replacement for readability
    for char, repl in pathCharReplDict.items():
        varpath = varpath.replace(char, repl)

    # Removing other special characters
    varpath = ''.join([c for c in varpath if char_isAllowed(c)])
    varpath = varpath.replace(" ", "_")
    return varpath


def prepinputs(x, y, w, cfgset, library="np", randomstate=None):
    """Peparing inputs and separating them into train and test sets

    :param x: inputs data
    :param y: targets
    :param w: weights
    :returns: dictionary with train and validation sets

    """
    if randomstate is None:
        randomstate = np.random.randint(1000, size=1)[0]
    xtrain, xval, ytrain, yval = train_test_split(x,
                                                  y,
                                                  test_size=cfgset.get("MODEL").get("ValidationSize"),
                                                  random_state=randomstate)
    _, _, wtrain, wval = train_test_split(x,
                                          w,
                                          test_size=cfgset.get("MODEL").get("ValidationSize"),
                                          random_state=randomstate)
    if library == "pd":
        xtrain = pd.DataFrame(xtrain, columns=[v.get("Name") for v in cfgset.get("VARIABLE")])
        xval = pd.DataFrame(xval, columns=[v.get("Name") for v in cfgset.get("VARIABLE")])
        ytrain = pd.DataFrame(ytrain, columns=["Label"])
        yval = pd.DataFrame(yval, columns=["Label"])
        wtrain = pd.DataFrame(wtrain, columns=["Weight"])
        wval = pd.DataFrame(wval, columns=["Weight"])
    return {"xtrain": xtrain,
            "xval": xval,
            "ytrain": ytrain,
            "yval": yval,
            "wtrain": wtrain,
            "wval": wval}
