void NN_Region_Study_2D(){
  /* Define Parameters */
  std::string treename        = "nominal_Loose";
  std::string NtuplesLocation = "/home/steffen/Documents/My_PhD/ttZ/Ntuples/ttZ_3L_v2_nominal_only_MVA_Reweighted/"; // location where your Ntuples are stored
  std::string FakeSum         = "(fake_1lep+fake_2lep+fake_3lep)==0";
  std::string NotFakeSum      = "(fake_1lep+fake_2lep+fake_3lep)!=0";
  std::string Selection       = "(tight_1lep && tight_2lep && tight_3lep && pT_1lep>27 && pT_2lep>10 && pT_3lep>10 && (mz1<101.1876 && mz1>81.1876) && nEl+nMu==3 && min_mll>10 && nBJets85>=1&&nJets>=3)";
  std::string MCWeight        = "(XSecWeight*weight_mc*weight_pileup*weight_leptonSF*weight_bTagSF_DL1r_Continuous*weight_jvt*weight_year*138965.2)";
  std::string OutputFname     = "CutsCalculated.txt";
  std::vector<std::string> Classifier{"ttZ_multi_class_Classifier_v3","ttZ_multi_class_Classifier_v4","ttZ_multi_class_Classifier_v5"}; // Classifier names to be read from ntuple file

  int Nbins = 100;
  float binwidth = 1.0/Nbins;
  
  /* Outputfile */
  ofstream outfile;
  outfile.open (OutputFname.c_str(), ios::trunc);
  outfile <<"Classifier" << "\t" << "Diboson Cut" << "\t" << "tZ Cut" << "\t" << "ttZ_SR" << "\t" << "ttZ_tZ_CR" << "\t" << "ttZ_WZ_CR" << "\t" << "ttW_SR" << "\t" << "ttW_tZ_CR" << "\t" << "ttW_WZ_CR" << "\t" << "ttH_SR" << "\t" << "ttH_tZ_CR" << "\t" << "ttH_WZ_CR" << "\t" << "WZb_SR" << "\t" << "WZb_tZ_CR" << "\t" << "WZb_WZ_CR" << "\t" << "WZc_SR" << "\t" << "WZc_tZ_CR" << "\t" << "WZc_WZ_CR" << "\t" << "WZl_SR" << "\t" << "WZl_tZ_CR" << "\t" << "WZl_WZ_CR" << "\t" << "ZZb_SR" << "\t" << "ZZb_tZ_CR" << "\t" << "ZZb_WZ_CR" << "\t" << "ZZc_SR" << "\t" << "ZZc_tZ_CR" << "\t" << "ZZc_WZ_CR" << "\t" << "ZZl_SR" << "\t" << "ZZl_tZ_CR" << "\t" << "ZZl_WZ_CR" << "\t" << "tZ_SR" << "\t" << "tZ_tZ_CR" << "\t" << "tZ_WZ_CR" << "\t" << "tWZ_SR" << "\t" << "tWZ_tZ_CR" << "\t" << "tWZ_WZ_CR" << "\t" << "Fakes_SR" << "\t" << "Fakes_tZ_CR" << "\t" << "Fakes_WZ_CR" << "\t" << "Other_SR" << "\t" << "Other_tZ_CR" << "\t" << "Other_WZ_CR" << std::endl;

  /* TChains */
  TChain ttZChain(treename.c_str());
  TChain ttWChain(treename.c_str());
  TChain ttHChain(treename.c_str());
  TChain WZChain(treename.c_str());
  TChain ZZChain(treename.c_str());
  TChain tZChain(treename.c_str());
  TChain tWZChain(treename.c_str());
  TChain FakesChain(treename.c_str());
  TChain OtherChain(treename.c_str());
  
  //Signal Samples
  std::string ttZ410156 = NtuplesLocation+"ttZ/exported*410156*";
  std::string ttZ410157 = NtuplesLocation+"ttZ/exported*410157*";
  std::string ttZ410218 = NtuplesLocation+"ttZ/exported*410218*";
  std::string ttZ410219 = NtuplesLocation+"ttZ/exported*410219*";
  std::string ttZ410220 = NtuplesLocation+"ttZ/exported*410220*";

  //ttW
  std::string ttW410155 = NtuplesLocation+"ttX/exported*410155*";

  //ttH
  std::string ttH34644 = NtuplesLocation+"ttX/exported*34644*";

  //WZ
  std::string WZ364253 = NtuplesLocation+"diboson/exported*364253*";
  std::string WZ364284 = NtuplesLocation+"diboson/exported*364284*";

  //ZZ
  std::string ZZ345705 = NtuplesLocation+"diboson/exported*345705*";
  std::string ZZ345706 = NtuplesLocation+"diboson/exported*345706*";
  std::string ZZ364250 = NtuplesLocation+"diboson/exported*364250*";
  std::string ZZ364288 = NtuplesLocation+"diboson/exported*364288*";

  //tZ
  std::string tZ412063 = NtuplesLocation+"tZ/exported*412063*";

  //tWZ
  std::string tWZ412118 = NtuplesLocation+"tWZ/exported*412118*";

  //Fakes_2b3j
  std::string Fakes410472 = NtuplesLocation+"fakes/exported*410472*";
  std::string Fakes3661   = NtuplesLocation+"fakes/exported*3661*";
  std::string Fakes3641   = NtuplesLocation+"fakes/exported*3641*";

  //Other
  std::string Other304014 = NtuplesLocation+"Other/exported*304014*";
  std::string Other342284 = NtuplesLocation+"Other/exported*342284*";
  std::string Other342285 = NtuplesLocation+"Other/exported*342285*";
  std::string Other363358 = NtuplesLocation+"Other/exported*363358*";
  std::string Other363359 = NtuplesLocation+"Other/exported*363359*";
  std::string Other363360 = NtuplesLocation+"Other/exported*363360*";
  std::string Other36424  = NtuplesLocation+"Other/exported*36424*";
  std::string Other410080 = NtuplesLocation+"Other/exported*410080*";
  std::string Other410081 = NtuplesLocation+"Other/exported*410081*";
  
  /* Filling TChains */
  
  //ttZ
  ttZChain.Add(ttZ410156.c_str());
  ttZChain.Add(ttZ410157.c_str());
  ttZChain.Add(ttZ410218.c_str());
  ttZChain.Add(ttZ410219.c_str());
  ttZChain.Add(ttZ410220.c_str());

  //ttW
  ttWChain.Add(ttW410155.c_str());

  //ttH
  ttHChain.Add(ttH34644.c_str());

  //WZb
  WZChain.Add(WZ364253.c_str());
  WZChain.Add(WZ364284.c_str());

  //ZZ
  ZZChain.Add(ZZ345705.c_str());
  ZZChain.Add(ZZ345706.c_str());
  ZZChain.Add(ZZ364250.c_str());
  ZZChain.Add(ZZ364288.c_str());

  //tZ
  tZChain.Add(tZ412063.c_str());

  //tWZ
  tWZChain.Add(tWZ412118.c_str());

  //Fakes
  FakesChain.Add(Fakes410472.c_str());
  FakesChain.Add(Fakes3661.c_str());
  FakesChain.Add(Fakes3641.c_str());

  //Other
  OtherChain.Add(Other304014.c_str());
  OtherChain.Add(Other342284.c_str());
  OtherChain.Add(Other342285.c_str());
  OtherChain.Add(Other363358.c_str());
  OtherChain.Add(Other363359.c_str());
  OtherChain.Add(Other363360.c_str());
  OtherChain.Add(Other36424.c_str());
  OtherChain.Add(Other410080.c_str());
  OtherChain.Add(Other410081.c_str());

  TCanvas *C1 = new TCanvas("c1","c1",600,800);
  for(int i=0; i<Classifier.size(); i++)
    {
      // Signal
      TH2F *ttZ = new TH2F("ttZ","ttZ", Nbins,0,1,Nbins,0,1);
      ttZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>ttZ").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      // Background
      TH2F *ttW = new TH2F("ttW","ttW", Nbins,0,1,Nbins,0,1);
      ttWChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>ttW").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH2F *ttH = new TH2F("ttH","ttH", Nbins,0,1,Nbins,0,1);
      ttHChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>ttH").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH2F *WZb = new TH2F("WZb","WZb", Nbins,0,1,Nbins,0,1);
      WZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>WZb").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets>0)").c_str());
      
      TH2F *WZc = new TH2F("WZc","WZc", Nbins,0,1,Nbins,0,1);
      WZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>WZc").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets>0)").c_str());
      
      TH2F *WZl = new TH2F("WZl","WZl", Nbins,0,1,Nbins,0,1);
      WZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>WZl").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets==0)").c_str());
      
      TH2F *ZZb = new TH2F("ZZb","ZZb", Nbins,0,1,Nbins,0,1);
      ZZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>ZZb").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets>0)").c_str());
      
      TH2F *ZZc = new TH2F("ZZc","ZZc", Nbins,0,1,Nbins,0,1);
      ZZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>ZZc").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets>0)").c_str());
      
      TH2F *ZZl = new TH2F("ZZl","ZZl", Nbins,0,1,Nbins,0,1);
      ZZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>ZZl").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+"&& nTruthBjets==0 && nTruthCjets==0)").c_str());
      
      TH2F *tZ = new TH2F("tZ","tz", Nbins,0,1,Nbins,0,1);
      tZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>tZ").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH2F *tWZ = new TH2F("tWZ","tWZ", Nbins,0,1,Nbins,0,1);
      tWZChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>tWZ").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());
      
      TH2F *Fakes = new TH2F("Fakes","Fakes", Nbins,0,1,Nbins,0,1);
      FakesChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>Fakes").c_str(),(MCWeight+"*("+Selection+"&&"+NotFakeSum+")").c_str());
      
      TH2F *Other = new TH2F("Other","Other", Nbins,0,1,Nbins,0,1);
      OtherChain.Draw((Classifier[i]+"_Diboson:"+Classifier[i]+"_tZ>>Other").c_str(),(MCWeight+"*("+Selection+"&&"+FakeSum+")").c_str());

      TFile* RootOutFile = new TFile((Classifier[i]+".root").c_str(),"RECREATE");
      ttZ->Write();
      ttH->Write();
      WZb->Write();
      WZc->Write();
      WZl->Write();
      ZZb->Write();
      ZZc->Write();
      ZZl->Write();
      tZ->Write();
      tWZ->Write();
      Fakes->Write();
      Other->Write();
      RootOutFile->Close();
      delete RootOutFile;

      for(int j = 1; j<=Nbins;j++){
	for(int k = 1; k<=Nbins;k++){
	  outfile << Classifier[i] << "\t"<< j*binwidth << "\t" << k*binwidth << "\t" << ttZ->Integral(0,k,0,j) << "\t" << ttZ->Integral(k,Nbins,0,j) << "\t" << ttZ->Integral(0,k,j,Nbins) << "\t" << ttH->Integral(0,k,0,j) << "\t" << ttH->Integral(k,Nbins,0,j) << "\t" << ttH->Integral(0,k,j,Nbins) << "\t" << ttW->Integral(0,k,0,j) << "\t" << ttW->Integral(k,Nbins,0,j) << "\t" << ttW->Integral(0,k,j,Nbins) << "\t" << WZb->Integral(0,k,0,j) << "\t"  << WZb->Integral(k,Nbins,0,j) << "\t" << WZb->Integral(0,k,j,Nbins) << "\t" << WZc->Integral(0,k,0,j) << "\t" << WZc->Integral(k,Nbins,0,j) << "\t" << WZb->Integral(0,k,j,Nbins) << "\t" << WZl->Integral(0,k,0,j) << "\t" << WZl->Integral(k,Nbins,0,j) << "\t" << WZl->Integral(0,k,j,Nbins) << "\t" << ZZb->Integral(0,k,0,j) << "\t" << ZZb->Integral(k,Nbins,0,j) << "\t" << ZZb->Integral(0,k,j,Nbins) << "\t" << ZZc->Integral(0,k,0,j) << "\t" << ZZc->Integral(k,Nbins,0,j) << "\t" << ZZc->Integral(0,k,j,Nbins) << "\t" << ZZl->Integral(0,k,0,j) << "\t" << ZZl->Integral(k,Nbins,0,j) << "\t" << ZZl->Integral(0,k,j,Nbins) << "\t" << tZ->Integral(0,k,0,j) << "\t" << tZ->Integral(k,Nbins,0,j) << "\t" << tZ->Integral(0,k,j,Nbins) << "\t" << tWZ->Integral(0,k,0,j) << "\t" << tWZ->Integral(k,Nbins,0,j) << "\t" << tWZ->Integral(0,k,j,Nbins) << "\t" << Fakes->Integral(0,k,0,j) << "\t" << Fakes->Integral(k,Nbins,0,j) << "\t" << Fakes->Integral(0,k,j,Nbins) << "\t" << Other->Integral(0,k,0,j) << "\t" << Other->Integral(k,Nbins,0,j) << "\t" << Other->Integral(0,k,j,Nbins) << "\n";
	}
      }

      /* Clean up */
      delete ttZ;
      delete ttH;
      delete ttW;
      delete WZb;
      delete WZc;
      delete WZl;
      delete ZZb;
      delete ZZc;
      delete ZZl;
      delete tZ;
      delete tWZ;
      delete Fakes;
      delete Other;
    }
  outfile.close();
  delete C1;
  gApplication->Terminate();
}
