import pandas as pd
import numpy as np
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Yield reference checking.')
    parser.add_argument('--file', dest='in_file', type=str)
    parser.add_argument('--reference', dest='ref_file', type=str)
    args = parser.parse_args()
    in_df = pd.read_hdf(args.in_file)
    ref_df = pd.read_csv(args.ref_file)
    print("Input file:")
    print(in_df.head(100))
    print("Reference file:")
    print(ref_df)
    for col, t in zip(ref_df, ref_df.dtypes):
        if col == "Label":
            if any([~((np.isnan(r) and np.isnan(i)) or abs(r-i)<10e-5) for r,i in zip(ref_df[col].values, in_df[col].head(100).values)]):
                print("Input file does not agrees with reference! Test FAILED!")
                exit(1)
        elif t==object or t==bool:
            if(any(ref_df[col].values!=in_df.head(100)[col].values)):
                print("Input file does not agrees with reference! Test FAILED!")
                exit(1)
        elif any(abs(ref_df[col].values-in_df.head(100)[col].values)>10e-5):
            print("Input file does not agrees with reference! Test FAILED!")
            exit(1)
    print("Input file agrees with reference. Test PASSED.")
    exit(0)
