# Dense Neural Network

Dense neural networks, also known as fully connected neural networks, are a type of artificial neural network where each neuron in one layer is connected to every neuron in the following layer. They are commonly used in deep learning for various applications, such as image and speech recognition, natural language processing, and predictive analytics. In a dense neural network, the input is passed through a series of hidden layers, where each layer learns increasingly complex data features before producing an output. With the help of backpropagation and optimisation algorithms, the network's weights and biases are adjusted to minimise a predefined loss function.

In MVA-trainer, dense neural networks are realised using [PyTorch](https://pytorch.org/).

PyTorch is a popular open-source machine learning library for Python used to develop and train deep learning models. Developed by Facebook's artificial intelligence research group, PyTorch is widely used by researchers and developers for its ease of use.

Information to get started with PyTorch can be found [here](https://pytorch.org/get-started/locally/).

## Defining the Architecture

When it comes to deep neural networks, several essential terms are commonly used to describe the architecture of a network. The most important terms include hidden layers, nodes, activation functions, dropout layers, and batch normalisation layers.

A neural network consists of one or more hidden layers responsible for processing the input data and producing an output. Each hidden layer comprises a series of nodes, which perform calculations on the input data and pass the result to the next layer.
A typical layour is sketched in the figure below.

![NN_layout](../img/NN_layout.png)

*Nodes* are individual units within a neural network that receive input from the previous layer and produce output for the next layer. Each node performs a weighted sum of the input data, applies an activation function to the result, and passes the output to the next layer.

*Activation functions* are used within nodes to introduce nonlinearity into the network output. They are used to model complex relationships between the input and output data and ensure that the network output is bounded and can be easily interpreted.

*Dropout layers* reduce overfitting in a neural network by randomly dropping out a percentage of nodes during training. This helps prevent the network from becoming too specialised in the training data and ensures that it can generalise to new data.

*Batch normalisation layers* are used to normalise the input data to each layer of the network, improving the stability and performance of the network. They work by normalising the output of each layer to have a mean of 0 and a standard deviation of 1, making it easier for the network to learn the appropriate weights and biases for each layer.

The first step of building a dense neural network is defining its architecture.
For this purpose, several tools are in place in MVA-trainer.
The architecture of a dense neural network is mainly defined by the following:

* the number of layers
* the number of nodes in each layer
* The activation functions of the layers
* where regularisation layers such as `dropout` or `batch normalisation` is applied.

In the `DNNMODEL` block, a dense neural network can be easily realised.
In the following, we will look at the example of a simple classifier with three layers with 50, 40 and 30 nodes each:

```
DNNMODEL
    Name = "My_Classifier"
    Type = Classification-DNN
    Nodes = 50,40,30
    ActivationFunctions = "relu","tanh","sigmoid"
    BatchNormIndece = 1,2
    DropoutIndece = 2,3
    OutputSize = 1
    OutputActivation = "sigmoid"
```
In the block, the name and type of the classifier are defined using the `name` and `type` options.
The `Nodes` option expects a list of integers which (in this example) defines the number of nodes per hidden layer.
The `ActivationFunctions` option defines which activation functions are used in the hidden layers.
In this example, `relu`, `tanh`, and `sigmoid` are used.
`BatchNormIndece` and `DropoutIndece` describe the number of hidden layers, after which either `dropout` or `batch normalisation` layers are added for regularisation.
The `OutputSize` and `OutputActivation` options define the shape and activation function of the output layer and hence determine whether the classifier is a binary or multi-class classifier.

## Early Stopping

In machine learning, *Early Stopping* is a technique used to prevent overfitting of the model on the training data by monitoring the validation loss during the training process. The idea is to stop the training process early, before the model overfits, by selecting the best-performing model based on the validation loss. The idea is schematically sketched in the figure below:

![Early Stopping](../img/Early_Stopping.png)

Two critical parameters used in early stopping are `Patience` and `MinDelta`.

Patience describes the number of epochs the model waits before stopping the training process if there is no improvement in the validation loss. If the validation loss does not improve after several epochs, the model assumes it has reached its optimal performance and stops the training process. The value of patience should be adjusted based on the dataset and the complexity of the model.
`MinDelt` is the minimum change in the validation loss that is considered an improvement. The training process is stopped if the validation loss does not improve by at least `MinDelta` after `Patience` epochs. `MinDelta` prevents the model from stopping too early due to small fluctuations in the validation loss.

Early stopping can significantly improve the model's generalisation performance by preventing overfitting. It is a simple yet effective technique.

## Hyperparameter optimisation

Hyparameter optimisation is a crucial aspect of training a good neural network. 
Currently, hyperparameter optimisation can be performed for deep neural networks.
The following parameters steer the hyperparameter optimisation:

* MaxLayers
* MinLayers
* MaxNodes
* MinNodes
* StepNodes
* SetActivationFunctions

To run hyperparameter optimisation, you can use the `--optimise` flag for MVA-trainer.
```sh
python3 python/mva-trainer -c <config_path> -optimisationpath <HTCondorConfig_path> --HO_options <HO_options> --NModels <NModels> --RandomSeed <RandomSeed>
```
The script will take the config file specified by `config_path` (and the corresponding options) as a "seed config" and create `NModels`, new randomised (control seed using `RandomSeed`) configs in the `HTCondorConfig_path` directory. Furthermore, `.sub` (and `.dag`) files are created.
Three `HO_options` are available to the user:

| Option | Effect |
| ------ | ------ |
| Converter | In this case, a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the conversion (in this case, only the conversion is run), submit the `.sub` file to the HTCondor daemon using the `condor_submit` command.|
| **Trainer (Default)** | In this case, a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the training (in this case, only the training is run), submit the `.sub` file to the HTCondor daemon using the `condor_submit` command. **For this job to work, converted files must already exist.**|
| Evaluater | In this case, a `.sub` file is created in the specified `HTCondorConfig_path` directory. To start the evaluation (in this case, only the evaluation is run), submit the `.sub` file to the HTCondor daemon using the `condor_submit` command. **For this job to work, the training step must have been executed beforehand.**|
| All |  In this case, a `.dag` file is created in the specified `HTCondorConfig_path` directory. In addition, two `.sub` files are created, one for the conversion and one for the training. The files are created so that the conversion only runs once per optimisation. To start optimising (in this case, a conversion followed by training all networks), submit the `.dag` file to the HTCondor daemon using the `condor_submit_dag` command.|
