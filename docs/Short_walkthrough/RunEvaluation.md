# Run an evaluation

The most important step after a successfull training is the evaluation of the model. This step is vital for finding problems and improving on previous results.
During the evaluation several additional plots are produced and stored in the `Plots` directory. They are grouped into different categories.

To evaluate a training/model you can do:
```sh
python3 python/mva-trainer.py -c <config_path> --evaluate
```
Here `<config_path>` is again the path to your config file and `--evaluate` tells the code to produce evaluation plots.

If you want to restrict yourself to only a subset of plots or if you want to reproduce dedicated plots you can use the `--plots` command line argument to only produce these plots.
By default all plots are created unless specified otherwise.
At the moment you can specify which plots you want for classification models. For other models by default all plots will be created.