# Boosted Decision Trees

Decision trees are a machine-learning model that can be used for regression and classification tasks. Decision trees represent a hierarchical structure of decisions that can be used to classify or predict the value of a target variable based on the values of input features.
The training of decision trees involves recursively splitting the data into subsets based on the values of input features to maximise the purity of the resulting subsets. The decision tree algorithm chooses the feature and threshold that separates the target variable values in the subsets based on a predefined metric. The process is repeated until the tree reaches a predefined depth, or no further improvement can be made in the purity of the subsets.
However, a single decision tree may only sometimes be the most accurate or robust model, especially when dealing with noisy or complex data. Boosted decision trees, such as Scikit-learn's [GradientBoostingClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingClassifier.html)), are a powerful extension of decision trees that can improve their accuracy and generalisation by combining the predictions of multiple weak decision trees.
`GradientBoostingClassifier` is a widespread implementation of boosted decision trees in Scikit-learn. It works by training a sequence of decision trees that each aim to correct the errors of the previous tree. At each stage, the `GradientBoostingClassifier` adjusts the weights of the training examples to emphasise those misclassified by the previous tree. The final model combines the predictions of all the decision trees to produce a final output.

## Building a BDT

In mva-trainer the architecture of a BDT is steered by the following parameters:

* nEstimators
* MaxDepth
* MaxFeatures
* MinSamplesSplit
* MinSamplesLeaf

See the settings for explanations of these parameters.

## Hyperparameter optimisation

Hyperparameter optimisation can also be performed for BDTs in MVA-trainer.
However, since the model in question is already an ensemble model, the optimised parameters will likely not improve the models performance significantly.
The following parameters steer the hyperparameter optimisation:

* MaxDepth
* MaxFeatures

Here these values are taking as maximum values in the random initialisation.
This means that new configs are assigned a random value between 1 and the defined value.

To run hyperparameter optimisation, you can use the `--optimise` flag for MVA-trainer.
```sh
python3 python/mva-trainer -c <config_path> -optimisationpath <HTCondorConfig_path> --HO_options <HO_options> --NModels <NModels> --RandomSeed <RandomSeed>
```

For an explanation of the options check the DNN optimisation section.
