# K-Folding: A Technique for Better Model Training and Evaluation

When training machine learning models, evaluating their performance on data that was not used for training is crucial. One way to do this is by using k-folding, a technique that involves dividing the dataset into k equal subsets, or "folds," using one as the validation set while the others are used for training.

Let us say we have a dataset with `n` events and want to train `k` models on this dataset. To use k-folding, we would first divide the dataset into `k` folds, where `k` is a positive integer. Each fold contains `n/k` events.

Next, we train `k` models, each using a different combination of `k-1` folds for training and the remaining fold for testing. For example, if we were using 5-folding (k=5), we would train five models as follows:

    Model 1: train on folds 2-5, test on fold 1
    Model 2: train on folds 1, 3-5, test on fold 2
    Model 3: train on folds 1-2, 4-5, test on fold 3
    Model 4: train on folds 1-3, 5, test on fold 4
    Model 5: train on folds 1-4, test on fold 5

Once we have trained the models, we can evaluate the events using the model foreseen to test this particular fold.                                                                            
The k-folding procedure is also visualised in the picture below. In addition, the validation set is shown in light blue.
It is drawn from the dark blue train set. It is used to monitor the validation loss throughout the training. If `Patience` and `MinDelta` is defined, it will be used to determine when to stop the training of a fold.

![kfolding](../img/kfolding.png) 