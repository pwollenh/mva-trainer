# Virtual environment on Lxplus

In the following we will discuss how to setup a python3 virtual environment (*venv*) on lxplus to use `mva-trainer`.
The setup should work ony any system with access to `cvmfs`.

First we have to setup the typical ATLAS environment:
```
setupATLAS
```
Now we setup `python3` and `ROOT` versions.
This is done in one single command:
```
lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"
```
Next we instantiate our venv called `myenv` and *start* it:
```
python3 -m venv myenv
source myenv/bin/activate
```

Next we need to reset the `PYTHONPATH` to point at the `PYTHONPATH` in the virtual environment.
Additionally we add the `ROOT` libraries to it.
```
export PYTHONPATH=$VIRTUAL_ENV/lib64/python3.9/:$ROOTSYS/lib
```
The next step consists of installing and upgrading `pip`
```
python -m ensurepip --upgrade
```
Please verify that `pip` is installed and working.
Now we continue installing the required packages in the environment:
```
pip install --no-cache-dir skl2onnx # will also install scikit-learn
pip install --no-cache-dir uproot
pip install --no-cache-dir pandas
pip install --no-cache-dir tables
pip install --no-cache-dir torch
pip install --no-cache-dir torchvision torchaudio
pip install --no-cache-dir torch_geometric
pip install --no-cache-dir pydot
pip install --no-cache-dir matplotlib
```

Now we check if we can access `ROOT`.
```
root --version
```
You should now see a `ROOT` command prompt showing version 6.26.08.

Now you can use this environment to run the scripts.

If you want to leave the venv do:
```
deactivate
```
The next time you are running the venv you don't need to reinstall the packages again. Instead you just have to activate it.